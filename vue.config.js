module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/hanged/' : '/',
  pages: {
    index: {
      // entry for the page
      entry: 'src/main.js',
      // the source template
      template: 'public/index.html',
      // output as dist/index.html
      filename: 'index.html',
      title: 'Impiccato'
    }
  },
  lintOnSave: process.env.NODE_ENV !== 'production'
}
